## Available filters

Customization is possible by adding filters to themes functions.php.

```php
<?php
add_filter('sf_seo_block_name', fn() => 'SEO-Daten');
add_filter('sf_seo_block_category', fn() => 'wurzer');
```
