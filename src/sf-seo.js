const { __ } = wp.i18n;
const {
    registerBlockType,
} = wp.blocks;


const {
    TextControl,
    TextareaControl,
    Button
} = wp.components;

const {
    MediaUpload,
} = wp.editor;

registerBlockType( 'sf/seo', {
    attributes: {
        title: {
            type: 'text'
        },
        description: {
            type: 'text'
        },
        imageUrl: {
            type: 'images'
        }
    },

    edit (props) {

        const { attributes, setAttributes, isSelected } = props;

        return (
            <div className={ props.className }>
                <h2>SEO-Daten</h2>

                <div class="components-base-control">
                    <label class="components-base-control__label">Foto</label>
                    <br/>

                    { ('undefined' != typeof attributes.imageUrl && attributes.imageUrl) &&
                    <span><img src={ attributes.imageUrl } style={ { 'max-height': '100px' } } /><br/></span>
                    }

                    { ('undefined' == typeof attributes.imageUrl || !attributes.imageUrl) &&
                    <i>Kein Foto gewählt</i>
                    }

                    <br/>
                    <MediaUpload
                        label={ 'Foto' }
                        onSelect={image => setAttributes({imageUrl: image.sizes.full.url})}
                        type="image"
                        render={ ( { open } ) => (
                            <button className="button" onClick={ open }>
                                Foto wählen
                            </button>
                        ) }
                    />

                    { ('undefined' != typeof attributes.imageUrl && attributes.imageUrl) &&
                    <Button isSecondary onClick={ () => setAttributes({imageUrl: null}) } style={ { 'margin-top': '3px' } }>
                        Foto entfernen
                    </Button>
                    }

                    <br/>
                    <br/>
                </div>

                <TextControl
                    label={ 'Titel' }
                    help={ 'Der Seitentitel wird verwendet wenn dieses Feld nicht befüllt ist' }
                    value={ attributes.title }
                    onChange={ value => setAttributes( { title: value } ) }
                />

                <TextareaControl
                    label={ 'Beschreibung' }
                    value={ attributes.description }
                    onChange={ value => setAttributes( { description: value } ) }
                />
            </div>
        )
    },

    save( props ) {
        return (
            <div className={ props.className }>
            </div>
        )
    }
});
