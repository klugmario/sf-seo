<?php
/**
 * Plugin Name: SF SEO
 * Description: Gutenberg-Block für SEO-Daten
 * Author: Mario Klug <mario.klug@sourcefactory.at>
 * Author URI: https://sourcefactory.at
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

add_action( 'init', function () {
    wp_register_script(
        'sf-seo-block-js',
        plugins_url( '/sf-seo/dist/sf-seo.js', dirname( __FILE__ ) ),
        ['wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor'],
        filemtime( plugin_dir_path( __DIR__ ) . 'sf-seo/dist/sf-seo.js' ),
        true // Enqueue the script in the footer.
    );

    register_block_type('sf/seo', [
        'title'         => apply_filters('sf_seo_block_name', 'SEO-Daten'),
        'category'      => apply_filters('sf_seo_block_category', 'sourcefactory'),
        'icon'          => 'admin-site-alt',
        'api_version'   => 2,
        'editor_script' => 'sf-seo-block-js',
        'editor_style'  => 'sf-seo-block-editor-css',
    ]);
});

add_action('wp_head', function() {
    $post = get_queried_object();

    $blocks = parse_blocks($post->post_content);

    foreach ($blocks as $block) {
        if ('sf/seo' === $block['blockName']) {
            $attrs = $block['attrs'];

            if ($attrs['imageUrl']) {
                printf('<meta property="og:image" content="%s">', $attrs['imageUrl']) . "\n";
            }

            if ($attrs['title']) {
                printf('<meta property="og:title" content="%s">', $attrs['title']) . "\n";
            }

            if ($attrs['description']) {
                printf('<meta property="og:description" content="%s">', $attrs['description']) . "\n";
            }
        }
    }
});
